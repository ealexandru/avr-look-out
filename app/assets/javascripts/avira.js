$(document).ready(function(){
    var urlParam = getUrlParameter('webcam')
    if (urlParam != null)
    {
        webcam(urlParam);
    }

    $('#submit').click(function(){
        selectValue = $('#webcam-select option:selected').val()
        if (selectValue != "0" || selectValue != null) {
            window.location.replace("/?webcam=" + selectValue)
        } else {
            console.log("not a valid selection");
        }
    })
}); 

function webcam(value){
    if (value == "0" || value == null){
        console.log("not a valid selection");
    } else {
        $.ajax({ type: 'GET',
            dataType: 'text/html',
            url: "/" + value,
            success: function(data){
                $('#webcam-image').html('<img src="' + data + '"')
            }
        });
    }
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}          