Rails.application.routes.draw do
  root 'webcam#index'

  get '/' => 'webcam#index'

  get '/webcam1' => 'webcam#webcam1'
  get '/webcam2' => 'webcam#webcam2'
end
